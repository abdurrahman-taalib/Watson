#!/bin/bash

# see https://en.wikipedia.org/wiki/Xvfb#Remote_control_over_SSH

DISP=${DISPLAY:1}

openbox &

Xvfb -screen $DISP ${CUSTOM_XVFB_WxHxD:=1080x720x16} -ac -pn -noreset &

VNC_PORT=$(expr 5900 + $DISP)
NOVNC_PORT=$(expr 6080 + $DISP)

x11vnc -localhost -shared -display :$DISP -forever -rfbport ${VNC_PORT} -bg -o "/tmp/x11vnc-${DISP}.log"
cd /opt/novnc/utils && ./launch.sh --vnc "localhost:${VNC_PORT}" --listen "${NOVNC_PORT}" &
